# jenkins-hw

1. Был установлен jenkins на вм   
2. Был установлен плагин для работы с ansible    
![ansible_plugin](screen/ansible_plugin.png)  
3. Был написан пайплайн, скачивающий ansible скрипт из репозитория и запускающий его.    
Но для начала в jenkins были введены необходимые ключи.  
Для гитлаба:  
![key-gitlab](screen/key-gitlab.png)    
Для соединения с вм, на которой будет располагаться minecraft сервер:  
![key-vm](screen/key-vm.png)  
Джоба была сделана параметризованной для ввода разных ip  
![parameterized](screen/parameterized.png)  
Сам пайплайн: 
``` 
node {
    stage('Cleanup') {
        cleanWs()
    }
    
    stage ('Git') {
        git branch: 'main', 
        url: "git@gitlab.com:PavVlada/minecraft-serv.git", 
        credentialsId: 'ALL-ssh'
    }
    
    stage ('Deploy') {
        ansiblePlaybook playbook: 'playbooks/minecraft_pb.yaml', 
        extras: "-e ansible_host=${ip} -v", 
        disableHostKeyChecking: true,
        inventory: 'hosts', 
        credentialsId: 'vm-ssh'
    }
}
```
В первом стейдже происходит очистка workspace, т.к. при повторном запуске пайплайна удаляется с этой вм скачанный репозиторий.  
По втором стейдже происходит скачивание ветки репозитория с использованием ключа, введенного ранее.  
В третьем стейдже происходит запуск плейбука и развертывание сервера minecraft на удаленную вм с использованием ключа.  
`DisableHostKeyChecking` - скрытие проверки подлинности хоста (т.к. не можем ввести 'yes').  
С помощью `extras` вводим ip адрес удаленной вм по введенному параметру.

Проверка того, что все запустилось и работает:  
 ![game](screen/game.jpg) 
