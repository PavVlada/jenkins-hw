node {
    stage('Cleanup') {
        cleanWs()
    }
    
    stage ('Git') {
        git branch: 'main', 
        url: "git@gitlab.com:PavVlada/minecraft-serv.git", 
        credentialsId: 'ALL-ssh'
    }
    
    stage ('Deploy') {
        ansiblePlaybook playbook: 'playbooks/minecraft_pb.yaml', 
        extras: "-e ansible_host=${ip} -v", 
        disableHostKeyChecking: true,
        inventory: 'hosts', 
        credentialsId: 'vm-ssh'
    }
}
